import React, {Component} from 'react';
import TabbedNavigator from './navigator/TabbedNavigator';
import configureStore from './components/Redux/store';
import {Provider} from 'react-redux';

const store = configureStore();

export default function App() {
  return (
    <Provider store={store}>
      <TabbedNavigator/>
    </Provider>
  );
}