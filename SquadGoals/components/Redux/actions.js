import database from '@react-native-firebase/database';

export const TOGGLE_MODAL = 'TOGGLE_MODAL';
export const FETCH_USER = 'FETCH_USER';
export const FETCH_ALL_GROUPS = 'FETCH_ALL_GROUPS';

export const setUserData = (userData) => {
  return {
    type: FETCH_USER,
    value: userData,
  };
};

export const fetchUser = () => {
  return function (dispatch) {
    database().ref('/user/0/')
    .once(
        'value',
        function (snapshot) {
          var userData = snapshot.val();
          dispatch(setUserData(userData));
        },
        function (error) {},
      );
  };
};

export const toggleModal = () => {
  return {
    type: TOGGLE_MODAL,
  };
};

export const setAllGroups = (allGroups) => {
  return {
    type: FETCH_ALL_GROUPS,
    value: allGroups,
  };
};

export const fetchAllGroups = (userData) => {
  return function (dispatch) {
    database()
      .ref('/user')
      .once(
        'value',
        function (snapshot) {
          var allGroupData = snapshot.val();

          var allGroups = [];
          let sports = [];
          let events = [];
          let hangouts = [];
          let internetGroups = [];
          let sections = [];

          let userId = userData._id;

          for (user of allGroupData) {
            if (userId != user._id) {
              for (group of user.groups) {
                allGroups.push(group);
              }
            }
          }

          for (let group of allGroups) {
            switch (group.groupType) {
              case 'event':
                events.push(group);
                break;

              case 'sport':
                sports.push(group);
                break;

              case 'hangout':
                hangouts.push(group);
                break;

              case 'internet':
                internetGroups.push(group);
                break;
            }
          }

          if (hangouts.length > 0) {
            sections.push({title: 'HANGOUT', data: hangouts});
          }

          if (internetGroups.length > 0) {
            sections.push({title: 'INTERNET', data: internetGroups});
          }

          if (events.length > 0) {
            sections.push({title: 'EVENT', data: events});
          }

          if (sports.length > 0) {
            sections.push({title: 'SPORT', data: sports});
          }

          dispatch(setAllGroups(sections));
        },
        function (error) {},
      );
  };
};
