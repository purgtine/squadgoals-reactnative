import { combineReducers } from 'redux';
import { TOGGLE_MODAl } from './actions'
import { FETCH_USER } from './actions'
import { FETCH_ALL_GROUPS } from './actions'

const INITIAL_STATE = {
  showModal: false,
  user: [],
  allGroups: []
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {  
    case FETCH_ALL_GROUPS:
      return {
        ...state,
        allGroups: action.value
      }

    case FETCH_USER:
      return {
        ...state,
        user: action.value
      }
    
    case TOGGLE_MODAl:
      return {
        ...state,
        showModal: !state.showModal
    }

    default:
      return state
  }
};

export default combineReducers({
  appReducer: reducer,
});