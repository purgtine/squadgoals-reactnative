import React from 'react';
import {Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import GroupsScreen from '../screens/Groups/groups';
import GroupFormScreen from '../screens/Groups/groupForm';
import FourSquaresScreen from '../screens/FourSquares/foursquares';
import ChatScreen from '../screens/Chat/chats';
import CalendarScreen from '../screens/Calendar/calendar';
import ProfileScreen from '../screens/Profile/profile';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import 'react-native-gesture-handler';
import Locations from '../screens/Locations/locations';
import Times from '../screens/Times/times';
import AddATime from '../screens/Times/addATime';
import Map from '../screens/Locations/locationMap';
import GroupInfo from '../screens/Groups/groupInfo';

const Tab = createBottomTabNavigator();

function TabbedNavigator() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;

            if (route.name === 'Groups') {
              iconName = focused ? 'list' : 'list';
            } else if (route.name === 'Calendar') {
              iconName = focused ? 'calendar' : 'calendar';
            } else if (route.name == 'Chats') {
              iconName = focused ? 'commenting' : 'commenting-o';
            } else if ((route.name = 'Profile')) {
              iconName = focused ? 'user-circle-o' : 'user-circle';
            }

            return (
              <Icon
                name={iconName}
                size={size}
                color={color}
                type="FontAwesome"
              />
            );
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}>
        <Tab.Screen name="Groups" component={GroupsStackScreen} />
        <Tab.Screen
          name="Calendar"
          component={CalendarStackScreen}
          initialParams={{props: this.props}}
        />
        <Tab.Screen name="Profile" component={ProfileStackScreen} />
        <Tab.Screen name="Chats" component={ChatsStackScreen} />   
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const options = {
  headerStyle: {
    backgroundColor: '#EC2929',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

const CalendarStack = createStackNavigator();

function CalendarStackScreen() {
  return (
    <CalendarStack.Navigator>
      <CalendarStack.Screen
        name="Calendar"
        component={CalendarScreen}
        options={options}
      />
    </CalendarStack.Navigator>
  );
}

const ChatsStack = createStackNavigator();

function ChatsStackScreen() {
  return (
    <ChatsStack.Navigator>
      <ChatsStack.Screen
        name="Chats"
        component={ChatScreen}
        options={options}
      />
    </ChatsStack.Navigator>
  );
}

const ProfileStack = createStackNavigator();

function ProfileStackScreen() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="Profile"
        component={ProfileScreen}
        options={options}
      />
    </ProfileStack.Navigator>
  );
}

const GroupsStack = createStackNavigator();

function GroupsStackScreen({navigation, props}) {

  const options = {
    headerStyle: {
      backgroundColor: '#EC2929',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    }
  };

  return (
    <GroupsStack.Navigator>
      <GroupsStack.Screen
        name="Groups"
        component={GroupsScreen}
        options={{
          headerStyle: {
            backgroundColor: '#EC2929',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold', 
          },
          headerRight: () => (
            <Button
              onPress={() => navigation.navigate('Add a group')}
              title="+"
              color="white"
            />
          ),
        }}
      />
      <GroupsStack.Screen
        name="Add a group"
        component={GroupFormScreen}
        options={options}
      />
      <GroupsStack.Screen
        name="fourSquares"
        component={FourSquaresScreen}
        options={{    
          headerStyle: {
          backgroundColor: '#EC2929',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
      />
      <GroupsStack.Screen
        name="groupInfo"
        component={GroupInfo}
        options={{    
          headerStyle: {
          backgroundColor: '#EC2929',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
      />
      <GroupsStack.Screen
        name="locations"
        component={Locations}
        options={{    
          headerStyle: {
          backgroundColor: '#EC2929',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        title: '6er',
        headerRight: () => (
          <Button
            onPress={() => navigation.navigate('Add a location')}
            title="+"
            color="white"
          />
        ),
      }}
      />
      <GroupsStack.Screen
        name="map"
        component={Map}
        options={{    
          headerStyle: {
          backgroundColor: '#EC2929',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
      />
      <GroupsStack.Screen
        name="times"
        component={Times}
        options={{    
          headerStyle: {
          backgroundColor: '#EC2929',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
       }}
      />
      <GroupsStack.Screen
        name="Add a time"
        component={AddATime}
        options={{    
          headerStyle: {
          backgroundColor: '#EC2929',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        title: '',
      }}
      />
    </GroupsStack.Navigator>
  );
}

export default TabbedNavigator;
