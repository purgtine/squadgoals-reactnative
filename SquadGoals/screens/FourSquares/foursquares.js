import React, {Component} from 'react';
import {View, Image, TouchableHighlight} from 'react-native';

class FourSquaresScreen extends Component {
  constructor(props) {
    super(props);
    this.props.navigation.setOptions({
      title: this.props.route.params.item.name,
    });

    const {item} = this.props.route.params;
    this.props.navigation.setParams({item2: item});
  }

  render() {
    const {item} = this.props.route.params;

    const Square = ({icon, navigation, navigateTo}) => {
      return (
        <TouchableHighlight
          onPress={() => navigation.navigate(navigateTo, {item})}
          underlayColor="transparent">
          <View
            style={{
              backgroundColor: 'red',
              padding: 45,
              margin: 20,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 20,
            }}>
            {icon === 0 && (
              <Image
                source={require('../../assets/images/near_me.png')}
                style={{tintColor: 'white', height: 60, width: 60}}
              />
            )}
            {icon === 1 && (
              <Image
                source={require('../../assets/images/chat_bubble.png')}
                style={{tintColor: 'white', height: 60, width: 60}}
              />
            )}
            {icon === 2 && (
              <Image
                source={require('../../assets/images/calendar_today.png')}
                style={{tintColor: 'white', height: 60, width: 60}}
              />
            )}
            {icon === 3 && (
              <Image
                source={require('../../assets/images/info.png')}
                style={{tintColor: 'white', height: 60, width: 60}}
              />
            )}
          </View>
        </TouchableHighlight>
      );
    };

    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column',
        }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-end',
            flexDirection: 'row',
          }}>
          <Square
            icon={0}
            navigation={this.props.navigation}
            navigateTo="locations"
          />
          <Square
            icon={1}
            navigation={this.props.navigation}
            navigateTo="chat"
          />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-start',
            flexDirection: 'row',
          }}>
          <Square
            icon={2}
            navigation={this.props.navigation}
            navigateTo="times"
          />
          <Square
            icon={3}
            navigation={this.props.navigation}
            navigateTo="groupInfo"
          />
        </View>
      </View>
    );
  }
}

export default FourSquaresScreen;
