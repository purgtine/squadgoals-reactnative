import React, {Component} from 'react';
import {StyleSheet, View, Text, SafeAreaView, SectionList,TouchableHighlight,Image} from 'react-native';
import {connect} from 'react-redux';

class ChatScreen extends Component {
  constructor(props) {
    super(props);

    this.props.navigation.setOptions({
      title: 'SquadGoals',
    });
  }

  render() {
    const {user} = this.props;

    const groups = user.groups;

    if (!groups) return <></>;

    let sports = [];
    let events = [];
    let hangouts = [];
    let internetGroups = [];

    let sections = [];
    for (let group of groups) {
      switch (group.groupType) {
        case 'event':
          events.push(group);
          break;

        case 'sport':
          sports.push(group);
          break;

        case 'hangout':
          hangouts.push(group);
          break;

        case 'internet':
          internetGroups.push(group);
          break;
      }
    }

    if (events.length > 0) {
      sections.push({title: 'DOGODEK', data: events});
    }

    if (sports.length > 0) {
      sections.push({title: 'ŠPORT', data: sports});
    }

    if (hangouts.length > 0) {
      sections.push({title: 'DRUŽENJE', data: hangouts});
    }

    if (internetGroups.length > 0) {
      sections.push({title: 'INTERNETNE SKUPINE', data: internetGroups});
    }

    const Item = ({item, navigation}) => (
      <TouchableHighlight
        onPress={() => navigation.navigate('fourSquares', {item})}
        underlayColor="white">
        <View style={styles.item}>
          <Text style={styles.title}>{item.name}</Text>
          <Image
            source={require('../../assets/images/keyboard_arrow_right.png')}
            style={styles.image}
          />
        </View>
      </TouchableHighlight>
    );

    return (
      <SafeAreaView>
        <View style={styles.container}>
          <Text style={styles.text}>Moji klepeti</Text>
        </View>
        <SectionList
          sections={sections}
          keyExtractor={(item, index) => item + index}
          renderItem={({item}) => (
            <Item item={item} navigation={this.props.navigation} />
          )}
          renderSectionHeader={({section: {title}}) => (
            <Text style={styles.header}>{title}</Text>
          )}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
  },
  text: {
    fontSize: 20,
    padding: 20,
    fontWeight: 'bold',
  },
  image: {
    alignSelf: 'flex-end',
  },
  header: {
    fontSize: 20,
    backgroundColor: '#E8E8E8',
    padding: 6,
  },
  title: {
    fontSize: 20,
    alignSelf: 'flex-start',
    padding: 6,
  },
  item: {
    backgroundColor: '#ffffff',
    borderBottomColor: '#E8E8E8',
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 6,
    padding: 7,
  },
});

const mapStateToProps = (state) => {
  return {
    user: state.appReducer.user,
  };
};

export default connect(mapStateToProps)(ChatScreen);
