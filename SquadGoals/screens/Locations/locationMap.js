import React, {Component} from 'react';
import {View, Text, Image, TouchableHighlight, FlatList} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import MapView, {Marker} from 'react-native-maps';

class Map extends Component {
  constructor(props) {
    super(props);
    this.props.navigation.setOptions({
      title: this.props.route.params.item.name,
    });
  }
  render() {
    const {item} = this.props.route.params;
    const {location} = this.props.route.params;

    console.log(location);

    return (
      <MapView
        style={{flex: 1}}
        showsUserLocation={true}
        region={{
          latitude: location.latitude,
          longitude: location.longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        }}>
        <Marker
          coordinate={{
            latitude: location.latitude,
            longitude: location.longitude,
          }}
        />
      </MapView>
    );
  }
}

export default Map;
