import React, {Component} from 'react';
import {View, Text, Image, TouchableHighlight, FlatList} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

class Locations extends Component {
  constructor(props) {
    super(props);
    this.props.navigation.setOptions({
      title: this.props.route.params.item.name,
    });
  }

  render() {
    const {item} = this.props.route.params;
    const {location} = item;

    const Item = ({item: i}) => {
      return (
        <TouchableHighlight
          onPress={() =>
            this.props.navigation.navigate('map', {item, location: i})
          }
          underlayColor="transparent">
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'row',
              marginVertical: 10,
              paddingBottom: 20,
              paddingHorizontal: 20,
              borderBottomColor: '#ddd',
              borderBottomWidth: 1,
            }}>
            <CheckBox style={{height: 20}} boxType="square" />
            <View style={{flex: 1}}>
              <Text style={{fontSize: 20}}>{i.city}</Text>
            </View>
            <Text style={{marginLeft: 'auto'}}>Votes: {i.votes}</Text>
            <Image
              source={require('../../assets/images/keyboard_arrow_right.png')}
              style={{tintColor: 'grey', height: 20, width: 20}}
            />
          </View>
        </TouchableHighlight>
      );
    };

    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-start',
          alignItems: 'stretch',
          flexDirection: 'column',
        }}>
        <Text style={{fontSize: 22, margin: 20, alignSelf: 'center'}}>
          Suggested Locations
        </Text>
        <FlatList
          data={location}
          renderItem={({item}) => (
            <Item item={item} navigation={this.props.navigation} />
          )}
        />
        <TouchableHighlight onPress={() => {}} underlayColor="transparent">
          <View
            style={{
              paddingVertical: 12,
              paddingHorizontal: 20,
              marginBottom: 20,
              borderRadius: 5,
              backgroundColor: 'grey',
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: 'white'}}>Vote</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

export default Locations;
