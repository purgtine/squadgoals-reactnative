import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class ProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.props.navigation.setOptions({
      title: "SquadGoals",
    });
  }

  render() {
    const {name, email} = this.props.userData;

    return (
      <View style={{backgroundColor: "#FFFFFF"}}>
         <View style={styles.title}>
          <Text style={styles.text}>Moj profil</Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <Icon
            style={{marginTop: 80}}
            name="user-circle"
            size={90}
            color="tomato"
            type="FontAwesome"
          />
        </View>
        <View style={{marginTop:"90%"}}>
          <View
            style={{
              marginHorizontal: 20,
              marginTop: 40,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text style={{fontWeight: 'bold', marginLeft: 20,fontSize:15}}>Ime:</Text>
            <Text style={{fontSize:15}}>{name.first}</Text>
          </View>
          <View
            style={{
              marginHorizontal: 20,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text style={{fontWeight: 'bold', marginLeft: 20, fontSize:15}}>Priimek:</Text>
            <Text style={{fontSize:15}}>{name.last}</Text>
          </View>
          <View
            style={{
              marginHorizontal: 20,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text style={{fontWeight: 'bold', marginLeft: 20, fontSize:15}}>Elektronski naslov:</Text>
            <Text style={{fontSize:15}}>{email}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
  },
  title: {
    alignContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    backgroundColor: '#FFFFFF',
  },
  text: {
    fontSize: 20,
    padding: 20,
    fontWeight: 'bold',
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.appReducer.user,
  };
};

export default connect(mapStateToProps)(ProfileScreen);
