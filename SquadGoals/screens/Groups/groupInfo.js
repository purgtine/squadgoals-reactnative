import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import ButtonWrapper from '../Groups/buttonWrapper'

class GroupInfo extends Component {
  constructor(props) {
    super(props);
    this.props.navigation.setOptions({
      title: this.props.route.params.item.name,
    });
  }

  render() {
    return (
      <View style={{fontSize: 300}}>
        <View style={{alignItems: 'center',marginTop:30}}>
        { this.props.user.groups.indexOf(this.props.route.params.item) < 0 &&
        <ButtonWrapper title="Pridruži">Hello</ButtonWrapper>
         }
        </View>
        <View
          style={{
            marginHorizontal: 20,
            marginTop: 40,
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Text style={{fontWeight: 'bold', marginLeft: 20}}>Ime skupine:</Text>
          <Text> {this.props.route.params.item.name}</Text>
        </View>
        <View style={{marginHorizontal: 20, marginTop: 20}}>
          <Text style={{fontWeight: 'bold', marginLeft: 20}}>
            Opis skupine:
          </Text>
          <Text style={{marginLeft:20}}>{this.props.route.params.item.description}</Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.appReducer.user,
  };
};

export default connect(mapStateToProps)(GroupInfo);
