import React, {Component} from 'react';
import {
  View,
  Text,
  Button,
  Modal,
  SectionList,
  StyleSheet,
  TextInput,
  Image,
  SafeAreaView,
  Picker,
  TouchableHighlight
} from 'react-native';
import {connect} from 'react-redux';
import {fetchAllGroups} from '../../components/Redux/actions';
import database from '@react-native-firebase/database';
import ButtonWrapper from '../Groups/buttonWrapper'

class GroupFormScreen extends Component {
  constructor(props) {
    super(props);

    this.props.fetchAllGroups(this.props.userData);
    this.state = {
      modalVisible: false,
      selectedValue: 'hangout',
      addGroup: false,
      nextPage: false,
      groupSubtype: '',
      groupName: '',
      lastPage: true,
      presentedGroups: this.props.allGroups,
      description: '',
    };

    this.addGroupClick = this.addGroupClick.bind(this);
    this.nextPageClick = this.nextPageClick.bind(this);
    this.searchClick = this.searchClick.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onChangeGroupName = this.onChangeGroupName.bind(this);
    this.onClickLastPage = this.onClickLastPage.bind(this);
    this.addGroupEndClick = this.addGroupEndClick.bind(this);
    this.searchEndClick = this.searchEndClick.bind(this);
    this.onChangeDescripton = this.onChangeDescripton.bind(this);
  }

  onChangeDescripton(text) {
    this.setState({
      description: text,
    });
  }

  onClickLastPage() {
    this.setState({
      lastPage: false,
    });
  }

  onChangeGroupName(text) {
    this.setState({
      groupName: text,
    });
  }

  onChangeText(text) {
    this.setState({
      groupSubtype: text,
    });
  }

  searchClick() {
    this.setState({
      modalVisible: true,
      addGroup: false,
    });
  }

  addGroupClick() {
    this.setState({
      modalVisible: true,
      addGroup: true,
    });
  }

  nextPageClick() {
    this.setState({
      nextPage: true,
    });
  }

  addGroupEndClick() {
    var groupNumb = this.props.userData.groups.length;

    database().ref('/user/0/groups/' + groupNumb)
      .set({
        chat: [{}],
        description: this.state.description,
        groupType: this.state.selectedValue,
        id: Date.now() + '',
        location: [{}],
        meeting: [{}],
        name: this.state.groupName,
      })
      .then(() => console.log('Data set.'));

    this.setState({
      modalVisible: false,
      selectedValue: 'hangout',
      addGroup: false,
      nextPage: false,
      groupSubtype: '',
      groupName: '',
      lastPage: true,
    });
  }

  searchEndClick() {
    var groups = [];
    var filtered = [];

    for (element of this.props.allGroups) {
      var data = element.data.map(function (item) {
        groups.push(item);
      });
    }

    for (group of groups) {
      if (this.state.selectedValue == group.groupType) {
        filtered.push(group);
      }
    }

    let sports = [];
    let events = [];
    let hangouts = [];
    let internetGroups = [];

    let sections = [];
    for (let group of filtered) {
      switch (group.groupType) {
        case 'event':
          events.push(group);
          break;

        case 'sport':
          sports.push(group);
          break;

        case 'hangout':
          hangouts.push(group);
          break;

        case 'internet':
          internetGroups.push(group);
          break;
      }
    }

    if (events.length > 0) {
      sections.push({title: 'DOGODEK', data: events});
    }

    if (sports.length > 0) {
      sections.push({title: 'ŠPORT', data: sports});
    }

    if (hangouts.length > 0) {
      sections.push({title: 'DRUŽENJE', data: hangouts});
    }

    if (internetGroups.length > 0) {
      sections.push({title: 'INTERNETNE SKUPINE', data: internetGroups});
    }

    this.setState({
      modalVisible: false,
      selectedValue: 'hangout',
      addGroup: false,
      nextPage: false,
      groupSubtype: '',
      groupName: '',
      lastPage: true,
      presentedGroups: sections,
    });
  }

  render() {
    const Item = ({item, navigation}) => (
      <TouchableHighlight
        onPress={() => navigation.navigate("groupInfo", {item})}
        underlayColor="white">
        <View style={styles.item}>
          <Text style={styles.title}>{item.name}</Text>
          <Image
            source={require('../../assets/images/keyboard_arrow_right.png')}
            style={styles.image}
          />
        </View>
      </TouchableHighlight>
    );

    return (
      <SafeAreaView>
        <View style={styles.container}>
          <Text style={styles.text}>Vse skupine</Text>
        </View>
        <SectionList
          style={{height: '80%'}}
          sections={this.state.presentedGroups}
          keyExtractor={(item, index) => item + index}
          renderItem={({item}) => <Item item={item} navigation={this.props.navigation} />}
          renderSectionHeader={({section: {title}}) => (
            <Text style={styles.header}>{title}</Text>
          )}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ButtonWrapper
            title="Išči"
            handleClick={this.searchClick}></ButtonWrapper>
          <ButtonWrapper
            title="Dodaj skupino"
            handleClick={this.addGroupClick}></ButtonWrapper>
        </View>
        <View>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                {!this.state.nextPage ? (
                  <>
                    <Picker
                      selectedValue={this.state.selectedValue}
                      style={{height: '20%', width: '70%', marginTop: '50%'}}
                      onValueChange={(itemValue, itemIndex) =>
                        this.setState({selectedValue: itemValue})
                      }>
                      <Picker.Item label="Druženje" value="hangout" />
                      <Picker.Item label="Šport" value="sport" />
                      <Picker.Item label="Dogodek" value="event" />
                      <Picker.Item label="Internetna skupina" value="internet" />
                    </Picker>
                    <View style={{marginTop: '90%'}}>
                      {(this.state.selectedValue != 'hangout' &&
                        this.state.addGroup == false) ||
                      this.state.addGroup == true ? (
                        <ButtonWrapper
                          handleClick={this.nextPageClick}
                          title="Naprej"
                        />
                      ) : (
                        <ButtonWrapper
                          handleClick={this.searchEndClick}
                          title="Najdi"
                        />
                      )}
                    </View>
                  </>
                ) : this.state.addGroup ? (
                  <View style={{width: '100%', alignItems: 'center'}}>
                    {this.state.lastPage ? (
                      <View
                        style={{
                          marginTop: '90%',
                          width: '100%',
                          alignItems: 'center',
                        }}>
                        <TextInput
                          onChangeText={(text) => this.onChangeGroupName(text)}
                          value={this.state.groupName}
                          style={styles.inputActivity}
                          placeholder="Vnesite ime skupine"></TextInput>

                        <SwitchForValue
                          onChangeText={(text) => this.onChangeText(text)}
                          value={this.state.groupSubtype}
                          activity={this.state.selectedValue}
                        />
                        <View style={{marginTop: '60%'}}>
                          <ButtonWrapper
                            handleClick={this.onClickLastPage}
                            title="Naprej"
                          />
                        </View>
                      </View>
                    ) : (
                      <>
                        <TextInput
                          multiline={true}
                          style={styles.inputDescription}
                          placeholder="Vnesite opis"
                          onChangeText={(text) => this.onChangeDescripton(text)}
                          value={this.description}
                        />

                        <ButtonWrapper
                          handleClick={this.addGroupEndClick}
                          title="Dodaj skupino"
                        />
                      </>
                    )}
                  </View>
                ) : (
                  <View
                    style={{
                      width: '100%',
                      alignItems: 'center',
                      marginTop: '90%',
                    }}>
                    <SwitchForValue
                      onChangeText={(text) => this.onChangeText(text)}
                      value={this.state.groupSubtype}
                      activity={this.state.selectedValue}
                    />
                    <View style={{marginTop: '80%'}}>
                      <ButtonWrapper
                        handleClick={this.searchEndClick}
                        title="Najdi"
                      />
                    </View>
                  </View>
                )}
              </View>
            </View>
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}

const SwitchForValue = (props) => {
  var placeholder = '';

  console.log(props.activity);

  switch (props.activity) {
    case 'sport':
      placeholder = 'Vnesite šport';
      break;

    case 'hangout':
      placeholder = 'Vnesite ime skupine';
      break;

    case 'event':
      placeholder = 'Vnesite ime dogodka';
      break;

    case 'internet':
      placeholder = 'Vnesite igro';
      break;
  }

  return (
    <TextInput
      {...props}
      style={styles.inputActivity}
      placeholder={placeholder}></TextInput>
  );
};

const styles = StyleSheet.create({
  inputDescription: {
    width: '100%',
    height: '80%',
    borderColor: 'gray',
    borderWidth: 2,
    margin: 10,
  },
  inputActivity: {
    height: '10%',
    width: '100%',
    borderColor: 'gray',
    borderWidth: 2,
    padding: 20,
    margin: 10,
  },
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  container: {
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
    padding: 20,
    fontWeight: 'bold',
  },
  image: {
    alignSelf: 'flex-end',
  },
  header: {
    fontSize: 20,
    backgroundColor: '#E8E8E8',
    padding: 6,
  },
  title: {
    fontSize: 20,
    alignSelf: 'flex-start',
    padding: 6,
  },
  item: {
    backgroundColor: '#ffffff',
    borderBottomColor: '#E8E8E8',
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 6,
    padding: 7,
  },
  centeredView: {
    alignItems: 'center',
    marginTop: '15%',
    height: '100%',
    width: '100%',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    width: '96%',
    height: '100%',
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

const mapStateToProps = (state) => {
  return {
    allGroups: state.appReducer.allGroups,
    userData: state.appReducer.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAllGroups: (userData) => dispatch(fetchAllGroups(userData)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GroupFormScreen);
