import React, {Component} from 'react';
import {Button, View} from 'react-native'

const ButtonWrapper = (props) => {
    const {title, handleClick} = props;
  
    return (
      <View
        style={{
          backgroundColor: 'gray',
          borderRadius: 15,
          margin: 20,
          width: 115,
          height: 50,
          justifyContent: 'center',
        }}>
        <Button title={title} color="white" onPress={handleClick} />
      </View>
    );
  };

   export default ButtonWrapper