import React, {Component} from 'react';
import {
  View,
  Text,
  SectionList,
  StyleSheet,
  SafeAreaView,
  TouchableHighlight,
  Image,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {fetchUser} from '../../components/Redux/actions';
import {SwipeListView} from 'react-native-swipe-list-view';
import database from '@react-native-firebase/database';

class GroupsScreen extends Component {
  constructor(props) {
    super(props);

    this.props.navigation.setOptions({
      title: 'SquadGoals',
    });
    this.props.fetchUser();
  }

  render() {
    const {user} = this.props;

    const groups = user.groups;

    if (!groups) return <></>;

    let sports = [];
    let events = [];
    let hangouts = [];
    let internetGroups = [];

    let sections = [];
    for (let group of groups) {
      switch (group.groupType) {
        case 'event':
          events.push(group);
          break;

        case 'sport':
          sports.push(group);
          break;

        case 'hangout':
          hangouts.push(group);
          break;

        case 'internet':
          internetGroups.push(group);
          break;
      }
    }

    if (events.length > 0) {
      sections.push({title: 'DOGODEK', data: events});
    }

    if (sports.length > 0) {
      sections.push({title: 'ŠPORT', data: sports});
    }

    if (hangouts.length > 0) {
      sections.push({title: 'DRUŽENJE', data: hangouts});
    }

    if (internetGroups.length > 0) {
      sections.push({title: 'INTERNETNE SKUPINE', data: internetGroups});
    }

    const Item = ({item, navigation}) => (
      <TouchableHighlight
        onPress={() => navigation.navigate('fourSquares', {item})}
        underlayColor="white">
        <View style={styles.item}>
          <Text style={styles.title}>{item.name}</Text>
          <Image
            source={require('../../assets/images/keyboard_arrow_right.png')}
            style={styles.image}
          />
        </View>
      </TouchableHighlight>
    );

    const deleteRow = (rowMap, rowKey) => {
      database().ref('/user/0/groups/'+rowKey).remove();
      this.props.fetchUser();
    };

    const closeRow = (rowMap, rowKey) => {
      if (rowMap[rowKey]) {
        rowMap[rowKey].closeRow();
      }
    };
    const renderHiddenItem = (data, rowMap) => (
      <View style={styles.rowBack}>
        <Text>Left</Text>
        <TouchableOpacity
          style={[styles.backRightBtn, styles.backRightBtnLeft]}
          onPress={() => closeRow(rowMap, data.item.id)}>
          <Text style={styles.backTextWhite}>Close</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.backRightBtn, styles.backRightBtnRight]}
          onPress={() => deleteRow(rowMap, data.item.id)}>
          <Text style={styles.backTextWhite}>Delete</Text>
        </TouchableOpacity>
      </View>
    );

    return (
      <SafeAreaView>
        <View style={styles.container}>
          <Text style={styles.text}>Moje skupine</Text>
        </View>
        <SwipeListView
          useSectionList
          sections={sections}
          renderItem={(data) => (
            <Item item={data.item} navigation={this.props.navigation} />
          )}
          renderHiddenItem={renderHiddenItem}
          renderSectionHeader={({section: {title}}) => (
            <Text style={styles.header}>{title}</Text>
          )}
          leftOpenValue={75}
          rightOpenValue={-75}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
  },
  text: {
    fontSize: 20,
    padding: 20,
    fontWeight: 'bold',
  },
  image: {
    alignSelf: 'flex-end',
  },
  header: {
    fontSize: 20,
    backgroundColor: '#E8E8E8',
    padding: 6,
  },
  title: {
    fontSize: 20,
    alignSelf: 'flex-start',
    padding: 6,
  },
  item: {
    backgroundColor: '#ffffff',
    borderBottomColor: '#E8E8E8',
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 6,
    padding: 7,
  },
  rowFront: {
    alignItems: 'center',
    backgroundColor: '#CCC',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    justifyContent: 'center',
    height: 50,
  },
  rowBack: {
    alignItems: 'center',
    backgroundColor: '#DDD',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
  },
  backRightBtnLeft: {
    backgroundColor: 'blue',
    right: 75,
  },
  backRightBtnRight: {
    backgroundColor: 'red',
    right: 0,
  },
});

const mapStateToProps = (state) => {
  return {
    user: state.appReducer.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUser: () => dispatch(fetchUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GroupsScreen);
