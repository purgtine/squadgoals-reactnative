import React, {Component} from 'react';
import {StyleSheet, Text, View, Alert} from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';
import {connect} from 'react-redux';
import moment from 'moment';

class CalendarScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedStartDate: null,
    };

    this.props.navigation.setOptions({
      title: 'SquadGoals',
    });

    this.onDateChange = this.onDateChange.bind(this);
    this.handleOnPressDay = this.handleOnPressDay.bind(this);
  }

  onDateChange(date) {
    this.setState({
      selectedStartDate: date,
    });
  }

  handleOnPressDay(day) {
    return Alert.alert(
      'Meeting',
      'My Alert Msg' + day,
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  }

  render() {
    const {selectedStartDate} = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';

    let customDatesStyles = [];

    for (let group of this.props.userData.groups) {
      for (let meeting of group.meeting) {
        let momentDate = moment(
          meeting.date,
          'DD-MMM-YYYY-H-mm-ss',
          true,
        ).format();
        customDatesStyles.push({
          date: momentDate,
          style: {backgroundColor: 'tomato'},
          textStyle: {color: 'black'},
        });
      }
    }

    return (
      <>
        <View style={styles.title}>
          <Text style={styles.text}>Moj koledar</Text>
        </View>
        <View style={styles.container}>
          <CalendarPicker
            todayTextStyle={{fontWeight: 'bold'}}
            onDateChange={this.onDateChange}
            customDatesStyles={customDatesStyles}
            handleOnPressDay={this.handleOnPressDay}
          />
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
  },
  title: {
    alignContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    backgroundColor: '#FFFFFF',
  },
  text: {
    fontSize: 20,
    padding: 20,
    fontWeight: 'bold',
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.appReducer.user,
  };
};

export default connect(mapStateToProps)(CalendarScreen);
