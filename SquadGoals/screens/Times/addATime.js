import React, {Component} from 'react';
import {View, Text, TouchableHighlight} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import {connect} from 'react-redux';
import database from '@react-native-firebase/database';

class AddATime extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: new Date(1598051730000),
    };
    this.onAddGroup = this.onAddGroup.bind(this)
  }

  onAddGroup(date) {
    let groupNumb = this.props.user.groups.indexOf(this.props.route.params.item)
    let meetingsLength = this.props.route.params.item.meeting.length

   database().ref('/user/0/groups/'+groupNumb+'/meeting/'+meetingsLength)
      .update({
        date: date.toString(),
        id: Date.now(),
        votes: 0
      })
      .then(() => console.log('Data set.'));
  }


  render() {
    let date = this.state.date

    const onChange = (event, selectedDate) => {
      const newTime = selectedDate || this.state.date;
      this.setState({
        date: newTime,
      });
    };

    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'stretch',
          flexDirection: 'column',
          paddingTop: 20,
        }}>
        <Text style={{fontSize: 22, margin: 20, alignSelf: 'center'}}>
          Choose a date
        </Text>
        <DateTimePicker
          testID="dateTimePicker"
          value={this.state.date}
          mode={'datetime'}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
        <TouchableHighlight
          onPress={this.onAddGroup(this.state.date)}
          underlayColor="transparent">
          <View
            style={{
              paddingVertical: 12,
              paddingHorizontal: 20,
              borderRadius: 5,
              backgroundColor: 'grey',
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: 'white'}}>Add Date</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.appReducer.user,
  };
};

export default connect(mapStateToProps)(AddATime);
