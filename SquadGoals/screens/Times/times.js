import React, {Component} from 'react';
import {View, Text, TouchableHighlight, FlatList} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import ButtonWrapper from '../Groups/buttonWrapper'

class Times extends Component {
  constructor(props) {
    super(props);
    this.props.navigation.setOptions({
      title: this.props.route.params.item.name,
    });
  }

  render() {
    const {item} = this.props.route.params;
    const {meeting} = item;

    const Item = ({item}) => {
      return (
        <TouchableHighlight onPress={() => {}} underlayColor="transparent">
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'row',
              marginVertical: 10,
              paddingBottom: 20,
              paddingHorizontal: 20,
              borderBottomColor: '#ddd',
              borderBottomWidth: 1,
            }}>
            <CheckBox style={{height: 20}} boxType="square" />
            <View style={{flex: 1}}>
              <Text style={{fontSize: 20}}>{item.date}</Text>
            </View>
            <Text style={{marginLeft: 'auto'}}>Glasovi: {item.votes}</Text>
          </View>
        </TouchableHighlight>
      );
    };

    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-start',
          alignItems: 'stretch',
          flexDirection: 'column',
        }}>
        <Text style={{fontSize: 22, margin: 20, alignSelf: 'center'}}>
          Predlagani časi
        </Text>
        <FlatList
          data={meeting}
          renderItem={({item}) => <Item item={item} />}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ButtonWrapper
            title="Glasuj"
            handleClick={this.handleClick}></ButtonWrapper>
          <ButtonWrapper
            title="Dodaj skupino"
            handleClick={() => this.props.navigation.navigate('Add a time', {item})}></ButtonWrapper>
            </View>
      </View>
    );
  }
}

export default Times;
